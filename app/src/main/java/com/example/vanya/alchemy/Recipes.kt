package com.example.vanya.alchemy

val images = mapOf(
        "fire" to R.drawable.fire,
        "water" to R.drawable.water,
        "dirt" to R.drawable.dirt,
        "steam" to R.drawable.steam,
        "cloud" to R.drawable.cloud,
        "geyser" to R.drawable.geyser,
        "earth" to R.drawable.earth,
        "lava" to R.drawable.lava,
        "vulcano" to R.drawable.vulcano,
        "obsidian" to R.drawable.obsidian,
        "stone" to R.drawable.stone,
        "clay" to R.drawable.clay,
        "brick" to R.drawable.brick,
        "powder" to R.drawable.powder,
        "air" to R.drawable.air,
        "life" to R.drawable.life,
        "human" to R.drawable.human,
        "fish" to R.drawable.fish,
        "food" to R.drawable.food,
        "nature" to R.drawable.nature,
        "thoughts" to R.drawable.thoughts,
        "energy" to R.drawable.energy,
        "lightning" to R.drawable.lightning,
        "atmosphere" to R.drawable.atmosphere

)
val pairs = mapOf(
        "water" to "fire" to "steam",
        "water" to "steam" to "cloud",
        "fire" to "steam" to "geyser",
        "earth" to "fire" to "lava",
        "earth" to "lava" to "vulcano",
        "water" to "lava" to "obsidian",
        "obsidian" to "fire" to "stone",
        "water" to "earth" to "clay",
        "clay" to "fire" to "brick",
        "vulcano" to "earth" to "powder",
        "earth" to "air" to "atmosphere",
        "atmosphere" to "water" to "life",
        "life" to "earth" to "human",
        "life" to "water" to "fish",
        "fish" to "fire" to "food",
        "earth" to "human" to "nature",
        "human" to "nature" to "thoughts",
        "thoughts" to "human" to "energy",
        "energy" to "air" to "lightning"
)
val discoveredItems = mutableSetOf<String>()