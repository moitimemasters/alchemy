package com.example.vanya.alchemy

import android.annotation.SuppressLint
import android.content.Context
import android.media.Image
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {
    class Element(val view: View, val name: String) {


        fun overlaps(second: Element): Boolean {

            if (view.left <= second.view.right && view.right >= second.view.right) {

                if (view.bottom <= second.view.top && second.view.top <= view.top) {

                    return true
                }
            }
            if (view.left <= second.view.left && view.right >= second.view.left) {

                if (view.bottom >= second.view.top && second.view.top >= view.top) {

                    return true

                }
            }
            if (view.left <= second.view.right && view.right >= second.view.right) {
                Log.v("main", "3")
                if (view.bottom >= second.view.bottom && second.view.bottom >= view.top) {
                    Log.v("main", "kek")
                    return true
                }
            }
            if (view.left <= second.view.left && view.right >= second.view.left) {

                if (view.bottom >= second.view.bottom && second.view.bottom >= view.top) {

                    return true
                }
            }
            return false
        }

        val x get() = view.left
        val y get() = view.top


    }


    val elements = mutableListOf<Element>()
    fun addElement(name: String, x: Int = 0, y: Int = 0): Element {
        discoveredItems+=name
        val imageView = ImageView(this)

        Log.v("test", "added ${name}")

        with(imageView) {
            setImageResource(images[name] ?: R.mipmap.ic_launcher_round)
            layoutParams = FrameLayout.LayoutParams(
                   75,75
            ).apply {
                leftMargin = x
                topMargin = y
            }

            vRoot.addView(this)
            scaleX = 0f
            scaleY = 0f

            ViewCompat.animate(this)
                    .setDuration(1000)
                    .scaleX(1f)
                    .scaleY(1f)
        }
        movable(imageView)
        val element = Element(imageView, name)
        elements += element
        return element
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        val fragment = SelectElementFragment()
        fragment.listener = {name ->
            addElement(name)
        }

        supportFragmentManager
                .beginTransaction()
                .add(R.id.vRoot, fragment, "selector")
                .commit()

        vAddElement.setOnClickListener {

            val selectorFragment = supportFragmentManager.findFragmentByTag("selector")
            if (selectorFragment.isHidden)

                supportFragmentManager
                        .beginTransaction()
                        .show(selectorFragment)
                        .commit()
            else
                supportFragmentManager
                        .beginTransaction()
                        .hide(selectorFragment)
                        .commit()
        }

        addElement("water", x = 10, y = 20)
        addElement("fire", x = 10, y = 105)
        addElement("earth",x=10,y=190)
        addElement("air", x=10,y = 280)
    }

    override fun onStart() {
        super.onStart()
        Log.v("main", "start")
    }

    override fun onPause() {
        super.onPause()
        Log.v("main", "pause")
    }

    override fun onStop() {
        super.onStop()
        Log.v("main", "stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("main", "destroy")
    }

    override fun onResume() {
        super.onResume()
        Log.v("main", "resume")
    }

    private fun movable(view: View) {
        var start_x = 0f
        var start_y = 0f

        view.setOnTouchListener { view, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    start_x = event.rawX
                    start_y = event.rawY
                }
                MotionEvent.ACTION_MOVE -> {
                    with(view.layoutParams as FrameLayout.LayoutParams) {
                        leftMargin += (event.rawX - start_x).toInt()
                        topMargin += (event.rawY - start_y).toInt()

                        view.requestLayout()
                    }
//                    view.offsetLeftAndRight((event.rawX - start_x).toInt())
//                    view.offsetTopAndBottom((event.rawY - start_y).toInt())

                    start_x = event.rawX
                    start_y = event.rawY
                }
                MotionEvent.ACTION_UP -> {
                    val toDel =  mutableListOf<Element>()
                    for (e in 0 until elements.size) {
                        for (e2 in e + 1 until elements.size) {
                            val el1 = elements[e]
                            val el2 = elements[e2]
                            if (el1.overlaps(el2)) {
//                                        Toast.makeText(this, el1.name + " collides " + el2.name, Toast.LENGTH_SHORT).show()
                                if ((el1.name to el2.name) in pairs || (el2.name to el1.name) in pairs) {
                                    val newName = pairs[(el1.name to el2.name)]
                                            ?: pairs[(el2.name to el1.name)]!!
                                    addElement(newName,(el1.x+el2.x)/2,(el1.y+el2.y)/2)
                                    toDel+=el1
                                    toDel+=el2





                                }
                            }

                        }
                    }
                    toDel.forEach{
                        elements-=it
                        vRoot.removeView(it.view)
                    }
                }

            }
            true
        }
    }
}